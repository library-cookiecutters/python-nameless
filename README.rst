========
Overview
========

An example package. Generated with cookiecutter-pylibrary.

Installation
============

::

    pip install nameless

You can also install the in-development version with::

    pip install https://gitlab.com/library-cookiecutters/python-nameless/-/archive/master/python-nameless-master.zip


Documentation
=============


http://library-cookiecutters.gitlab.io/python-nameless


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
